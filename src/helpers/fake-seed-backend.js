export function configureFakeSeedBackend() {        
    let seed_infos = [
        {id: 1, year: "2001", quality:"High", status:1, count:70},
        {id: 2, year: "2002", quality:"High", status:1, count:45},
        {id: 3, year: "2003", quality:"High", status:1, count:86},
        {id: 4, year: "2004", quality:"High", status:1, count:80},
        {id: 5, year: "2005", quality:"High", status:1, count:74},
        {id: 6, year: "2001", quality:"Low", status:1, count:34},
        {id: 7, year: "2002", quality:"Low", status:1, count:65},
        {id: 8, year: "2003", quality:"Low", status:1, count:31},
        {id: 9, year: "2004", quality:"Low", status:1, count:54},
        {id: 10, year: "2005", quality:"Low", status:1, count:44},
        {id: 11, year: "2001", quality:"High", status:2, count:89},
        {id: 12, year: "2002", quality:"High", status:2, count:94},
        {id: 13, year: "2003", quality:"High", status:2, count:32},
        {id: 14, year: "2004", quality:"High", status:2, count:88},
        {id: 15, year: "2005", quality:"High", status:2, count:67},
        {id: 16, year: "2001", quality:"Low", status:2, count:88},
        {id: 17, year: "2002", quality:"Low", status:2, count:37},
        {id: 18, year: "2003", quality:"Low", status:2, count:66},
        {id: 19, year: "2004", quality:"Low", status:2, count:77},
        {id: 20, year: "2005", quality:"Low", status:2, count:85},
    ]

    let realFetch = window.fetch;
    window.fetch = function(url, opts) {
        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(() => {
                /*SEED INFORMATION*/
                if (url.endsWith('/seed/seed_info') && opts.method === 'GET') {     
                    let params = JSON.parse(opts.body);                
                    var temp = []
                    seed_infos.map(seed_info => {
                        temp.push(seed_info)
                    })
                    var new_seed_infos = temp
                    resolve({ ok: true, json: () => new_seed_infos });
                    return;
                }
                // update second-evaluation
                if (url.endsWith('/seed/seed_info') && opts.method === 'POST') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);
                    var temp = []
                    seed_infos.map(seed_info => {
                        if (seed_info.quality === params.quality && seed_info.status === parseInt(params.status)) {
                            temp.push(seed_info)
                        }
                        else if(params.quality === "" && params.status === "")
                        {
                            temp.push(seed_info)
                        }
                        else if(params.status === "")
                        {
                            if(params.quality === seed_info.quality)
                            {
                                temp.push(seed_info)
                            }
                        }
                        else if(params.quality === "" )
                        {
                            if(parseInt(params.status) === seed_info.status)
                            {
                                temp.push(seed_info)
                            }
                        }
                    })
                    var new_seed_infos = temp
                    resolve({ ok: true, json: () => new_seed_infos });
                    return;
                }

                if (url.endsWith('/seed/seed_info') && opts.method === 'PUT') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);
                    var temp = []
                    seed_infos.map(seed_info => {
                        if (seed_info.id === params.seed_info.id) {
                            seed_info = params.seed_info
                        }
                        temp.push(seed_info)
                    })
                    seed_infos = temp
                    resolve({ ok: true, json: () => seed_infos });
                    return;
                }
                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));
            }, 100);
        });
    };
}
