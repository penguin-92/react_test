import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { fetchJSON } from '../../helpers/api';

import {
    GET_SEED_INFORMATION,
    FILTER,
    FILTER_FAIL,
    FILTER_SUCCESS
} from './constants'

import {
    filterFailed, filterSuccess,
    getSeedInformationSuccess, getSeedInformationFailed,
} from './actions'

function* filter({payload:{quality, status}}) {
    const options = {
        body: JSON.stringify({quality, status}),
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
    };

    console.log("options = ", options)
    try {
        const response = yield call(fetchJSON, '/seed/seed_info', options);
        yield put(filterSuccess(response));
    } catch (error) {
        let message;
        switch (error.status) {
            case 500:
                message = 'Internal Server Error';
                break;
            case 401:
                message = 'Invalid credentials';
                break;
            default:
                message = error;
        }
        yield put(filterFailed(message));
    }
}

/*SEED INFORMATION*/ 
function* getSeedInformation({ payload: {name} }) {
    const options = {
        body: JSON.stringify({ name }),
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };

    try {
        const response = yield call(fetchJSON, '/seed/seed_info', options);
        console.log("response = ", response)
        yield put(getSeedInformationSuccess(response));
    } catch (error) {
        let message;
        switch (error.status) {
            case 500:
                message = 'Internal Server Error';
                break;
            case 401:
                message = 'Invalid credentials';
                break;
            default:
                message = error;
        }
        yield put(getSeedInformationFailed(message));
    }
}

export function* watchFilter(): any {
    yield takeEvery(FILTER, filter);
}

/*SEED INFORMATION*/ 
export function* watchGetSeedInformation(): any {
    yield takeEvery(GET_SEED_INFORMATION, getSeedInformation);
}

function* seedSaga(): any {
    yield all([
        fork(watchFilter),

        /*SEED INFORMATION*/ 
        fork(watchGetSeedInformation),
    ]);
}
export default seedSaga;