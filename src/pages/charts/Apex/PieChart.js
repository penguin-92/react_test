// @flow
import React from 'react';
import Chart from 'react-apexcharts';
import { Card, CardBody } from 'reactstrap';

// simple pie chart
const getData = (data) =>{
    let values = [];
    if(data.data)
    {
        for(var i = 0; i < data.data.length; i++)
        {
            let value = data.data[i].count
            values.push(value)
        }
    }

    return values
}

function PieChart(props){
    const apexDonutOpts = {
        chart: {
            height: 320,
            type: 'pie',
        },
        // labels: ['2001', '2002', '2003', '2004', '2005', '2006', '2007'],
        // colors: ['#727cf5', '#6c757d', '#0acf97', '#fa5c7c', '#e3eaef', '#e3ea00', '#e309ef'],
        legend: {
            show: true,
            position: 'bottom',
            horizontalAlign: 'center',
            verticalAlign: 'middle',
            floating: false,
            fontSize: '14px',
            offsetX: 0,
            offsetY: -10,
        },
        responsive: [
            {
                breakpoint: 600,
                options: {
                    chart: {
                        height: 240,
                    },
                    legend: {
                        show: false,
                    },
                },
            },
        ],
    };

    const apexDonutData = [44, 55, 41, 17, 15, 16, 30];

    //const apexDonutData = props.data;
    return (
        <Card>
            <CardBody>
                <h4 className="header-title mb-3">Pie Chart</h4>
                <Chart options={apexDonutOpts} series={getData(props.data)} type="pie" height={320} className="apex-charts" />
            </CardBody>
        </Card>
    );
};

export default PieChart;
