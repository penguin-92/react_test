import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Card, CardBody, Button, FormGroup, Label, Input,
    UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
    Nav, NavItem, NavLink, TabContent, TabPane} from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import paginationFactory, { PaginationProvider, PaginationTotalStandalone,
    SizePerPageDropdownStandalone, PaginationListStandalone} from 'react-bootstrap-table2-paginator';
import classnames from 'classnames';
import LangUtils from '../../helpers/langUtils';
import PageTitle from '../../components/PageTitle';
import { ProductTypes, SelectAll, State, Units } from '../../helpers/constant';
import { getSeed, filter } from '../../redux/actions';
import LoaderWidget from '../../components/Loader';
import SeedPlantingInfo from './SeedPlantingInfo';
import PieChart from '../charts/Apex/PieChart';
import BarChart from '../charts/Apex/BarChart';
import LineChart from '../charts/Apex/LineChart';

class SeedInformation extends Component {
    _isMounted = false;

    constructor(props, context) {
        super(props, context);
      
        this.state = { activeTab: '1' };
        this.toggle = this.toggle.bind(this);
    }

    toggle = (tab: string) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab,
            });
        }
    };

    componentWillMount() {
        //this.props.getSeed('')  
    }
    
    componentDidMount() {
        this._isMounted = true;
        
        this.props.filter("", "")
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    
    render() {
        console.log("PROPS = ", this.props)
        const {user} = this.props  

        const tabContents = [
            {
                id: '1',
                title: "PIE CHART",
            },
            {
                id: '2',
                title: "BAR CHART",
            },
            {
                id: '3',
                title: "LINE WITH DATA LABELS",
            },
        ];

        return (
        
            <React.Fragment>
                <PageTitle
                    breadCrumbItems={[
                        { label: "", path: '/seed/info' },
                        { label: "", path: '/seed/info' },
                    ]}
                    title={""}
                />
    
                <Row>
                    <Col>
                        <PieChart data={this.props.seed_infos}/>
                    </Col>
                    <Col>
                        <BarChart data={this.props.seed_infos}/>
                    </Col>
                    <Col>
                        <LineChart data={this.props.seed_infos}/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                                <Nav tabs>
                                    {tabContents.map((tab, index) => {
                                            return (
                                                <NavItem key={index}>
                                                    <NavLink
                                                        href="#"
                                                        className={classnames({ active: this.state.activeTab === tab.id })}
                                                        onClick={() => {
                                                            this.toggle(tab.id);
                                                        }}>
                                                        <i
                                                            className={classnames(
                                                                tab.icon,
                                                                'd-lg-none',
                                                                'd-block',
                                                                'mr-1'
                                                            )}></i>
                                                        <span className="d-none d-lg-block">{tab.title}</span>
                                                    </NavLink>
                                                </NavItem>
                                            );
                                        })}
                                </Nav>
                                <TabContent activeTab={this.state.activeTab}>
                                    {tabContents.map((tab, index) => {
                                        return (
                                            <TabPane tabId={tab.id} key={index}>
                                                <Row>
                                                    <Col sm="12">
                                                        {index === 0 && <SeedPlantingInfo {...this.props}/>}
                                                        {index === 1 && <SeedPlantingInfo {...this.props}/>}
                                                        {index === 2 && <SeedPlantingInfo {...this.props}/>}
                                                    </Col>
                                                </Row>
                                            </TabPane>
                                        );
                                    })}
                                </TabContent>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }   
    
}
/**
     @params@
     //This sets the stats values to props while interactions. 
*/
const mapStateToProps = state => {
    return {
        user: state.Auth.user,
        loading: state.Seed.loading,
        seed_infos: state.Seed.seed_infos,
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        { getSeed, filter }
    )(SeedInformation)
);