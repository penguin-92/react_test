import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import { Container, Row, Col, 
    Card, CardBody, Label, 
    FormGroup, Button, Alert,
    CardImg, CardTitle, 
    CardHeader, CardSubtitle} from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';

import { loginUser } from '../../redux/actions';
import { isUserAuthenticated } from '../../helpers/authUtils';
import LoaderWidget from '../../components/Loader';

class Login extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);

        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.state = {
            email: 'test@gmail.com',
            password: '123456',
            verifyError: false,
        };

        this.RCGElement = React.createRef()

        this.check = this.check.bind(this)
        this.verifyTextChanged = this.verifyTextChanged.bind(this)
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    /**
     * Handles the submit
     */
    handleValidSubmit = (event, values) => {
        //event.preventDefault();
        console.log("Values = ", values)
        let checked = this.check()

        if(checked)
        {
            this.props.loginUser(values.email, values.password, this.props.history);
        }
    };

    /**
     * Redirect to root
     */
    renderRedirectToRoot = () => {
        const isAuthTokenValid = isUserAuthenticated();
        if (isAuthTokenValid) {
            return <Redirect to="/" />;
        }
    };

    check() {
        return true;
    }

    verifyTextChanged() {
        this.setState({verifyError: false})
    }

    render() {
        const isAuthTokenValid = isUserAuthenticated();
        return (
            <React.Fragment>
                {this.renderRedirectToRoot()}

                {(this._isMounted || !isAuthTokenValid) && (
                    <div className="account-pages mt-5 mb-5">
                        <Container>
                            <Row className="justify-content-center">
                                <Col lg={4} style={{alignSelf:'center'}}>
                                    <CardImg src={require('../../assets/images/img/login-pic.png')} />
                                </Col>
                                <Col lg={4} style={{alignSelf:'center'}}>
                                    <Card inverse style={{background:'transparent', width:'100%', height:'100%', boxShadow:'none'}}>
                                        <CardHeader style={{background:'transparent', borderColor:'#00c3ae'}}>
                                            <CardTitle tag='h3' className="pl-3">REACT</CardTitle>
                                            <CardSubtitle tag='h4' className="pl-5 pb-3"></CardSubtitle>
                                        </CardHeader>
                                        <CardBody>
                                            React test assignment
                                        </CardBody>
                                    </Card>
                                </Col>
                                <Col lg={4}>
                                    <Card style={{backgroundColor:'transparent', borderColor:'#0a72cd', borderWidth:'2px', color:'white'}}>
                                        {/* <div className="card-header pt-4 pb-4 text-center bg-primary">
                                            <Link to="/">
                                                <span>
                                                    <img src={logo} alt="" height="18" />
                                                </span>
                                            </Link>
                                        </div> */}

                                        <CardBody className="p-4 position-relative">
                                            {/* preloader */}
                                            {this.props.loading && <LoaderWidget />}

                                            <div className="text-center w-75 m-auto">
                                                <h4 className="text-dark-50 text-center mt-0 font-weight-bold mb-4">
                                                Login
                                                </h4>
                                                {/* <p className="text-muted mb-4">
                                                    Enter your email and password to access admin panel.
                                                </p> */}
                                            </div>

                                            {this.props.error && (
                                                <Alert color="danger" isOpen={this.props.error ? true : false}>
                                                    <div>{this.props.error}</div>
                                                </Alert>
                                            )}

                                            <AvForm onValidSubmit={this.handleValidSubmit}>
                                                {/* <AvField
                                                    name="email"
                                                    label="情输入账号/手机号"
                                                    placeholder="情输入账号/手机号"
                                                    value={this.state.email}
                                                    required
                                                /> */}
                                                <AvGroup>
                                                    <Label for="email">Your Email</Label>
                                                    <AvInput
                                                        type="email"
                                                        name="email"
                                                        id="email"
                                                        placeholder="email"
                                                        value={this.state.email}
                                                        required
                                                        style={{ backgroundImage:'none', border: 'none'}}
                                                    />
                                                    <AvFeedback>Invalid email</AvFeedback>
                                                </AvGroup>

                                                <AvGroup>
                                                    <Label for="password">Password</Label>
                                                    {/* <Link
                                                        to="/account/forget-password"
                                                        className="text-muted float-right">
                                                        <small>Forgot your password?</small>
                                                    </Link> */}
                                                    <AvInput
                                                        type="password"
                                                        name="password"
                                                        id="password"
                                                        placeholder="Password"
                                                        value={this.state.password}
                                                        required
                                                        style={{ backgroundImage:'none', border: 'none'}}
                                                    />
                                                    <AvFeedback>Invalid password</AvFeedback>
                                                </AvGroup>
                                                <FormGroup>
                                                    <Button className="btn-rounded mt-4 font-18" block color="success">Login</Button>
                                                </FormGroup>

                                                {/* <p>
                                                    <strong>email:</strong> test &nbsp;&nbsp;{' '}
                                                    <strong>Password:</strong> test
                                                </p> */}
                                            </AvForm>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                            {/* <Row> */}
                                
                            {/* </Row> */}
                            {/* <Row className="mt-1">
                                <Col className="col-12 text-center">
                                    <p className="text-muted">
                                        Don't have an account?{' '}
                                        <Link to="/account/register" className="text-muted ml-1">
                                            <b>Register</b>
                                        </Link>
                                    </p>
                                </Col>
                            </Row> */}
                        </Container>                        
                    </div>
                   
                )}
                {/* <div>
                    <CardImg src={require('../../assets/images/img/android-qrcode.png')} 
                    style={{position:'fixed', left: '20px',bottom: '0',width:'50px', height:'50px'}}/>
                </div> */}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    const { user, loading, error } = state.Auth;
    return { user, loading, error };
};

export default connect(
    mapStateToProps,
    { loginUser }
)(Login);
